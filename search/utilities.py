
import random
from search import probleme
from .debug_by_anes import *
import sys
"""
GENERATEUR DE CARTES par moi
pour une meilleure experiences et meilleure visualisation
des strategies
"""


def legal_position(row, col, wallStates, posPlayers,height):
    # une position legale est dans la carte et pas sur un mur
    basic_conds = ((row,col) not in wallStates) and row>=0 and row<height and col>=0 and col<height
    return basic_conds and (row, col) not in posPlayers


def random_move(current, wallStates, posPlayers, height):
    (row, col) = current
    while True:  # tant que pas legal on retire une position
        x_inc, y_inc = random.choice([(0, 1), (0, -1), (1, 0), (-1, 0)])
        next_row = row+x_inc
        next_col = col+y_inc
        if legal_position(next_row, next_col, wallStates, posPlayers, height):
            break
    return (next_row, next_col)


def mapGenerator(height=20, cardEquipe=1, obstacle_per=0.1):
    print(yellow, "--------------------------")
    print("          CARTE: ")
    print(yellow, "--------------------------")
    print(yellow, "Dimensions :", stop, height, "×", height)
    print(yellow, "Nb Jouerus par equipe :", stop, cardEquipe)
    print(yellow, "Pourcentage des obstacles :",
          stop, int(obstacle_per*100), "%")
    print(yellow, "--------------------------", stop)
    # height = _height or 20
    # obstacle_per = _obstacle_per or 0.1
    # cardEquipe = _cardEquipe or 1

    n = height*height
    matrix = ['o'] * int(obstacle_per * n)
    matrix += ['r'] * cardEquipe * 2
    matrix += ['j'] * cardEquipe * 2
    matrix += ['b'] * (n - len(matrix))
    random.shuffle(matrix)
    alt_j = alt_r = 0
    back_ground_list = [31, 32, 47, 48]
    obstacles_list = [9, 10, 11, 12, 13, 14, 15, 16, 25, 26, 89]
    joueur_list = [131, 199]  # human, phantom
    ramassables_list = [91, 94]  # maison, ROUGE
    for i, item in enumerate(matrix):
        if item == 'b':
            matrix[i] = random.choice(back_ground_list)
        elif item == 'o':
            matrix[i] = random.choice(obstacles_list)
        elif item == 'j':
            matrix[i] = joueur_list[alt_j % 2]
            alt_j += 1
        else:
            matrix[i] = ramassables_list[alt_r % 2]
            alt_r += 1

    # pour factoriser
    def print_common(list_, name_, id_, last_=None):
        if id_ != 1:
            print(list(map(lambda x: x if x in list_ else 0, matrix)))
        else:
            print(
                list(map(lambda x: x if x in list_ else back_ground_list[0], matrix)))
        print(',"height":'+str(height)+',"id":'+str(id_)+',"name":"'+name_ +
              '","opacity":1,"type":"tilelayer","visible":true,"width":'+str(height)+',"x":0,"y":0}', end='')
        print(',{"data":') if not last_ else print('')

    original_stdout = sys.stdout  # Save a reference to the original
    f = open("pySpriteWorld/Cartes/costumeMap.json", "w")
    sys.stdout = f  # Change the standard output to the file we created
    print('{"compressionlevel":-1, "height":'+str(height) +
          ', "infinite":false, "layers":[ { "data":')
    print_common(back_ground_list, "bg1", 1)
    print_common(obstacles_list, "obstacles", 2)
    print_common(ramassables_list, "ramassables", 3)
    print_common(joueur_list, "joueur", 4, True)
    # -------------------------------------
    print(
        '],"nextlayerid":5,"nextobjectid":1,"orientation":"orthogonal","renderorder":"right-down","tiledversion":"1.4.1","tileheight":32,"tilesets":[{"columns":16,"firstgid":1,"image":"\\/SpriteSheet-32x32\\/tiny_complete.bmp","imageheight":832,"imagewidth":512,"margin":0,"name":"tiny_complete","spacing":0,"tilecount":416,"tileheight":32,"tilewidth":32}],"tilewidth":32,"type":"map","version":1.4,"width":'+str(height)+'}')

    f.close()
    sys.stdout = original_stdout

equipe = {
    0: {
        "name": "Human",
        "color": cyan,
        "strategie": "path_slicing"
    },
    1: {
        "name": "Phantom",
        "color": red,
        "strategie": "path_slicing"
    }
}
 
def is_collision(instantT, indiceJoueur, caseEnQuestion, posPlayers, path):
    """
    retourne si oui ou non la caseEnQuestion est une case collision
    pour cela il faut savoir d'abord a quel instantT est le jeu,
    quel joueur veut savoir, et pour quelle case, ainsi que 
    la position de tout les joueurs et leurs path
    """
    # print("posPlayers"+str(enumerate(posPlayers)))
    # print("caseEnQuestion"+str(caseEnQuestion))
    # aucun joueur n'occupe la case voulue
    # print(caseEnQuestion)
    # print("tel que posPlayers[2]", posPlayers[2])
    # print(caseEnQuestion not in posPlayers)
    # print(caseEnQuestion == posPlayers[indiceJoueur])
    if (caseEnQuestion not in posPlayers or caseEnQuestion == posPlayers[indiceJoueur]):
        return False
    else:
        # index de joueur creant la collision qu'on appel JC
        ijc = posPlayers.index(caseEnQuestion)
        string = white+magentabg+"  COLLISION: case occuped par "+equipe[ijc%2]["name"]+str(ijc)+"  "+stop
        print(string)
        # input()
        return True


    # index de joueur creant la collision qu'on appel JC
    ijc = posPlayers.index(caseEnQuestion)
    # test(ijc)
    string = yellowbg+red+equipe[indiceJoueur % 2]["name"]+str(indiceJoueur)+" se trouvant sur "+str(posPlayers[indiceJoueur])
    string +="veut atteindre"+str(path[indiceJoueur][-1])
    string += "\nCollision a : "+str(caseEnQuestion)+"\n"
    # string += "C'est la case que le joueur se trouvant a "+str(posPlayers[indiceJoueur])+" veut acceder\n"
    # string += "en meme temps "
    #la prochaine case de JC
    # si la case en question est la dernniere case de JC
    # il va donc pas bouger, il y a collision
    # path[ijc][-1] == posPlayers[ijc]
    # try:
    #     pc_jc = path[ijc][instantT+1]
    # except:
    if path[ijc][-1] == posPlayers[ijc]:
        # cas ou c'est la case en question est l'objectif
        # de JC, donc y a collision
        string += "cette case est la case finale du"+equipe[ijc % 2]["name"]+str(ijc)+" ce dernier ne va donc pas bouger\n"
        string += "il faut donc trouver un nouveau path \n"
        print(string)
        return True
    
    # pc_jc : prochaine case du joueur collision
    try:
        pc_jc = path[ijc][instantT+1]
    except:
        test(ijc)
        instantT_1 = instantT+1
        test(instantT_1)
        path_ijc = path[ijc]
        test(path_ijc)
        # input("error")
        return True
    # sinon si il va la ceder mais qu'il va occuper la place actuelle de notre joueur
    # alors c'est le cas face a face 
    if (pc_jc == posPlayers[indiceJoueur]):
        string += "cette case sera liberer par "+equipe[ijc % 2]["name"]+str(posPlayers[ijc])+" cepednant ce dernier va occuper\n"
        string += "la case "+str(posPlayers[indiceJoueur])+" => tomonage face a face"
        print(string)
        return True
    return False


def hundle_recalcing(g, current, objectif, path, ite, height):
    tmp_g = g.copy()
    maybe = path[ite]
    # je considere la case collision comme un obstacle
    tmp_g[maybe] = False
    # je calcule le nouveau chemin a partir de la case sur laquelle je suis actuellement
    p = ProblemeGrid2D(current, objectif, tmp_g, 'manhattan')
    new_portion = probleme.astar(p)
    # print("new_portion BURUTE mel astar", new_portion)
    # si pas de path alors je vais me declarer bloquer par la suite
    if path[ite] == path[-1]:
        print("ma prochaine case est ma case finale")
        return [current]+[path[-1]]
        
    if (new_portion == []):  # was new_portion[1:] == []
        return

    if objectif != new_portion[-1]:
        # si je suis le nouveau path suggere je vais jamais arriver a mon objectif
        # je vais plutot temporiser
        print(red, "le chemin propose par hundle collision nest pas pratique")
        print(new_portion)
        print("car il mene pas a la cible (alors qu'avant si)")
        print("je temposrise alors et je laisse l'autres bouger")
        # path.insert(ite, current)
        # print(path)
        return []

    # si je sais que je vais jamais arriver alors je vise plutot de minimiser la distence de manhatten
    # sinon je supprime a partir de de ma case actuelle exlue
    # del path[(ite+1):]
    # print("path apres del", path)
    print(orange+"J'empreinte ce nouveau chemin pour eviter la collision:")
    print(new_portion,stop)
    # # j'attribue mon nouveau path
    # path += new_portion  # [1:]
    # print(path[ite:], stop)
    return new_portion
    # ok je compte aller sur la case maybe maintenant
    # maybe = row, col = path[ite]
    # si jamais y a un probleme je reboucle pour aller autres part


def hundle_slicing(g, current, objectif, path, ite, wallStates, posPlayers, height):
    """
    je renvoie un chemin reparer localement 
    ou un nouveau chemin vers l'objectif
    """
    tmp_g = g.copy()
    maybe = path[ite]
    # je considere la case collision comme un obstacle
    tmp_g[maybe] = False
    # je calcule le nouveau chemin a partir de la case sur laquelle je suis actuellement vers la prochaine case sur mon chemin (en evitant celle
    # de collision biensur)
    # si c la derniere case bouger, puis 
    if path[ite] == path[-1]:
        print("ma prochaine case est ma case finale")
        return [current]+[path[-1]]

    but = path[ite+1]
    # input()
    p = ProblemeGrid2D(current, but, tmp_g, 'manhattan')
    new_portion = probleme.astar(p)
    if path[ite] == path[-1]:
        new_portion = [current]+new_portion
    l_chemin_avec_detour = len(new_portion) + len(path[ite+1:]) - 1
    l_chemin_directe = len(path)
    print("j'essaye de detourner la collision")
    to_add = new_portion+path[ite+2:]
    # if(l_chemin_avec_detour > l_chemin_directe):
    #     print("je decale un peu et je reessaye")
    #     flag()
    #     random_case = random_move(current, wallStates, posPlayers, height)
    #     flag()
    #     return [random_case]+path[ite:]
    #     print("il vaut mieux que je recalcule un nouveau chemin directe vers l'objectif, pour voir")
    #     p = ProblemeGrid2D(current, objectif, tmp_g, 'manhattan')
    #     new_chemin = probleme.astar(p)
    #     l_chemin_directe = len(new_chemin)
    #     if(l_chemin_avec_detour > l_chemin_directe):
    #         print("effectivement, j'y vais directe")
    #         to_add = new_chemin

    if (to_add == []):  # was to_add[1:] == []
        return

    # if objectif != to_add[-1]:
    #     # si je suis le nouveau path suggere je vais jamais arriver a mon objectif
    #     # je vais plutot temporiser
    #     print(red, "le chemin propose par hundle collision nest pas pratique")
    #     print(to_add)
    #     print("car il mene pas a la cible (alors qu'avant si)")
    #     print("je temposrise alors et je laisse l'autres bouger")
    #     # path.insert(ite, current)
    #     # print(path)
    #     return []

    # si je sais que je vais jamais arriver alors je vise plutot de minimiser la distence de manhatten
    # sinon je supprime a partir de de ma case actuelle exlue
    # del path[(ite+1):]
    # print("path apres del", path)
    print(orange+"J'empreinte ce nouveau chemin pour eviter la collision:")
    print(to_add, stop)
    # # j'attribue mon nouveau path
    # path += to_add  # [1:]
    # print(path[ite:], stop)
    return to_add
    # ok je compte aller sur la case maybe maintenant
    # maybe = row, col = path[ite]
    # si jamais y a un probleme je reboucle pour aller autres part


# retourne le nombre de joueurs de lequipe i
# qui sont dans le tableau tab
def nb(tab):
    # c = 0
    # for elem in tab:
    #     if elem != 0:
    #         c += 1
    # t = :
    return len(list(filter(lambda x: x!= 0, tab)))

    # FONCTIONS AUXILIAIRES

def gagant(vinqeur, ok, bloked):
    _cardEquipe = len(ok) // 2
    for i in [0, 1]:
        n_ok = nb(ok[i::2])
        n_dist = sum(filter(lambda x: bloked.index(x) % 2 == i, bloked))
        n_blk = nb(bloked[i::2])
        n_encour = _cardEquipe - n_blk - n_ok
        print("___________________________________________")
        print("                  ",
              equipe[i]["name"].upper(), " ("+str(nb(ok[i::2]))+" /"+str(_cardEquipe)+")                ")
        print("⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻")
        print(green, "arrived |", yellow,
                "en cours |", red, "bloqued | dist")
        print(green, "   ", n_ok, " ", yellow, "     ", n_encour,
                " ", red, "     ", n_blk, "    ", n_dist, stop)
        print("⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻⎻")
    print(greenbg, "Les", equipe[vinqeur]["name"], "gagnent !!", stop)


def end(motif, ie, ok, bloked,game):
    game.mainiteration()
    verdict = None
    if (motif == "SANS-ISSUE" or motif == "OUT OF ITERATIONS"):
        print(
            yellow, "Les deux equipe ne peuvent plus atteindre leurs cibles")
        if nb(ok[0::2]) == nb(ok[1::2]):
            print(yellow, "EGALITE: chaque equipe a atteint",
                  nb(ok[0::2]), "cibles", stop)
            print(yellow, "on compare les distances")
            sum0 = sum(bloked[::2])
            sum1 = sum(bloked[1::2])
            verdict = 0 if sum0 <= sum1 else 1
        else:
            print(
                yellow, "Cependant, une equipe a atteint plus de cibles qu'une l'autre", stop)
            verdict = (0 if nb(ok[0::2]) >= nb(ok[1::2]) else 1)
    elif (motif == "WE-HAVE-A-WINNER"):
        verdict = ie
    gagant(verdict, ok, bloked)  # declarer le vinceur
    input("appuyer pour quitter")

"""
prend en entree un path et retourne en sortie
la portion du path jsuqua la case ayant la plus petite distence 
de la cible, ou la case actuelle si c'est la bonne  
"""

from search.probleme import *
from search.grid2D import *

def rapprochement(current, objectif, g):
    # je calcule le chemein vol de oiaseau
    p = ProblemeGrid2D(current, objectif, g, 'manhattan')
    new_cible = vol_oiseau(p)[1]
    # print(new_cible)
    # input()
    p = ProblemeGrid2D(current, new_cible, g, 'manhattan')
    path = probleme.astar(p)
    return path or [current]


    # flag()
    # print(path)
    # flag()
    # print("chemin vol de oiseau:")
    # print(path)
    # # print(path)
    # path = path[::-1] # reverse
    # # print(path)
    # for i, c in  enumerate(path):
    #     # si c'est un obsatcle passe au suivant 
    #     if g[c] == False:
    #         continue
    #     # je considere la case juste avant 
    #     # cible = c
    #     # except:
    #     #     return [current]
    #     # si je peux aller a cette case 
    #     print("jessaye de calcluer path entre ",current,"et",c)
    #     tt = astar(ProblemeGrid2D(current, c, g, 'manhattan'))
    #     print(tt)
    #     if tt[-1] == c:
    #         return tt
    #     else:
    #         print("ma ykharejch")
    # return [current]



def rapprochement_reserve(current, objectif, g):
    p = ProblemeGrid2D(current, objectif, g, 'manhattan')
    path = vol_oiseau(p)
    t = [current]+path
    tab_dis = list(map(lambda x: (x, probleme.distManhattan(x, objectif)), t))
    bonne_case, dis_min = tab_dis[0]
    good_index = 0
    for index, (x, y) in enumerate(tab_dis):
        if(y < dis_min):
            bonne_case = x
            dis_min = y
            good_index = index
    print("la case la plus proche du but est ",
          bonne_case, "avec une distance de ", dis_min)
    print(good_index)
    return [current]+t[:good_index]


# def hundle2(g, maybe, current, objectif):
#     tmp_g = g.copy()
#     # je considere la case collision comme un obstacle
#     tmp_g[maybe] = False
#     # je calcule le nouveau chemin a partir de la case sur laquelle je suis actuellement
#     p = ProblemeGrid2D(posPlayers[i], objectifs[i], tmp_g, 'manhattan')
#     new_portion = probleme.astar(p)
#     # si pas de path alors je vais me declarer bloquer par la suite

#     version = 2
#     # version pour les phantoms seulement
#     if (version == 2 and i % 2 == 1):
#         # je regarde il reste combien d'iteration
#         # iterations
#         # si jamais je vois que a la fin de jeu je n'attendrai pas envore
#         # ma cible sur mon nouveau chemin
#         # tmp_path = path[i][:ite]
#         # tmp_path += new_portion[1:]
#         old_new_portion = new_portion
#         if (len(new_portion) < (iterations - itera)):
#             print("le nouveau chemin en totale est ", new_portion)
#             # alors je considere rester sur ma place ou encore enprinter ce chemin jusque une certaine case
#             # ou rebrouser chemin, l'essentiel le but est d'avoir une distence de manhatten faible
#             # si je reste
#             # manhatten_rester =  probleme.distManhattan(current, objectifs[i])
#             # la meilleure case

#             tab_dis = list(map(lambda x: (x, probleme.distManhattan(
#                 x, objectifs[i])), [current]+new_portion))
#             print("... avec distance :", tab_dis)
#             bonne_case, dis_min = tab_dis[0]
#             index = 0
#             for index_, (x, y) in enumerate(tab_dis):
#                 if(y < dis_min):
#                     bonne_case = x
#                     dis_min = y
#                     index = index_
#             print("la meilleure case est:", bonne_case,
#                   "avec une dist de", dis_min)
#             son_index = index
#             print("index de la meilleure case :", son_index)
#             new_portion = new_portion[:son_index]
#             print("du coup je vais marcher jusqua : ")
#             print(green, new_portion, stop)
#             print("au lieu de  ")
#             print(red, old_new_portion, stop)
#             # input()

#     if (new_portion == []):  # was new_portion[1:] == []
#         break

#         # si je sais que je vais jamais arriver alors je vise plutot de minimiser la distence de manhatten
#     # sinon je supprime mon ancien path (a partir de la case surlaquelle suivante)
#     del path[i][ite:]
#     string = str(greenbg) + \
#         " j'empreinte ce nouveau chemin pour eviter la collision:\n"
#     print(string)
#     # j'attribue mon nouveau path
#     path[i] += new_portion  # [1:]
#     print(path[i][ite:], stop)
#     # ok je compte aller sur la case maybe maintenant
#     maybe = row, col = path[i][ite]
#     # si jamais y a un probleme je reboucle pour aller autres part

#     # try:
#     #     if path[i] != []:
#     #     else:
#     #         print(magentabg, "SEEMS BLOKED 1111")
#     # except:
#     #     print(magentabg, "SEEMS BLOKED 22222")
#     #     input()
#     # input()
