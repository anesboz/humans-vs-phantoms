"""
CE FICHIER sert UNIQUEMENT pour debuggage
il est utile qu'en phase de developpement, et il doit etre supprime en production
si vous etes arrivez a le voir c'est que j'ai oublier de le supprimer
Anes BOUZOUAOUI

########## A AJOUTER DANS LE "main" :
vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
from debug_by_anes import test, flag # TO BE DELETED
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

affiche la chaine "@@@@@@@@@@@@@" en couleur
utilite : savoir si l execution a atteint une ligne ou pas
en cas de bugs, possiblite de modifier la couleur
"""


# FOR DEBUG: afficher de facon lisible et le nom et la valeur d une variable
# TO DELETE AFTER (only usful in development mode)
def test(p):
    import inspect
    import re
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\btest\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            # print('\033[96m'+m.group(1)+':\033[0m '+p)
            print("\033[1;96m[\033[1;33m "+m.group(1)+" :\033[0;0m "+str(p)+"\033[1;96m ]\033[0;0m\n")


# green = "\033[1;32m"
# yellow = "\033[1;33m"
# red = "\033[1;31m"
# stop = "\033[0;0m"
stop = '\033[0;0m\u001b[0m'

# COLORS
black='\u001b[30;1m'
white='\u001b[0;0m'
red='\u001b[31;1m'
green='\u001b[32;1m'
yellow='\u001b[33;1m'
blue='\u001b[34;1m'
magenta='\u001b[35;1m'
cyan='\u001b[36;1m'
orange='\u001b[38;5;202m'

# BACKGROUND COLORS
blackbg='\u001b[40;1m'
redbg='\u001b[41;1m'
greenbg='\u001b[42;1m'
yellowbg='\u001b[43;1m'
bluebg='\u001b[44;1m'
magentabg='\u001b[45;1m'
cyanbg='\u001b[46;1m'
whitebg='\u001b[47;11m'

# FOR DEBUG: afficher de facon lisible un message
def flag(*arg) :
    try:
        color = arg[0]
    except :
        color = None
    if color == 1:
        print(yellow+"@@@@@@@@@@@@@"+stop)
    elif color == 2:
        print(red+"@@@@@@@@@@@@@"+stop)
    else:
        print(green+"@@@@@@@@@@@@@"+stop)


def stand():
    while True:
        pass
