from __future__ import absolute_import, print_function, unicode_literals
import random 
import numpy as np
import sys
from itertools import chain
import pygame

# Tout ce qui est dans le repertoire pySpriteWorld
# est du code fourni a ne pas modifier
# mise a part eventuellement le sous repertoire Cartes
from pySpriteWorld.gameclass import Game,check_init_game_done
from pySpriteWorld.spritebuilder import SpriteBuilder
from pySpriteWorld.players import Player
from pySpriteWorld.sprite import MovingSprite
from pySpriteWorld.ontology import Ontology
import pySpriteWorld.glo

# TO MODIFY with strategies 
# pour l'instant il n'y a que la strategie A-satr et pour un seul
# agent dans chaque equipe
from search.grid2D import ProblemeGrid2D
from search.probleme import *
from search.utilities import *
from search.debug_by_anes import * # TO BE DELETED

game = Game()

"""
constructeur du jeu a partir de la map entree
en parametre, 'demoMap' sinon.
pour modifier les maps il faut voir dans le repertoire
'psSpriteWorld/Cartes/'
"""
def init(_boardname=None):
    global player,game
    name = _boardname if _boardname is not None else 'demoMap'
    game = Game('Cartes/' + name + '.json', SpriteBuilder)
    game.O = Ontology(True, 'SpriteSheet-32x32/tiny_spritesheet_ontology.csv')
    game.populate_sprite_names(game.O)
    game.fps = 5  # frames per second was 5
    game.mainiteration()
    player = game.player


def main():
    iterations = 100 # default
    #./main.py 140 pour 140iterations
    if len(sys.argv) == 2:
        iterations = int(sys.argv[1])
    # RANDOM PARAMS
    height = random.randint(10, 22)
    _cardEquipe = random.randint(1, 8)
    _obstacle_per = random.randint(1, 55) / 100
    # FIXED PARMATS
    # height = 10
    # _cardEquipe = 4
    # _obstacle_per = 0.01
    # APPELL
    # ----------------------------------
    # mapGenerator(height, _cardEquipe, _obstacle_per)
    # init('costumeMap')
    # ----------------------------------
    # appler le constructeur avec la map definie
    strategie_1 = "path_slicing"
    strategie_2 = "path_recalcing"
    strategie_3 = "path_rapprochement"
    #-------------------------------
    # attribution des strategies aux equipes
    #-------------------------------
    equipe[0]["strategie"] = 1  # humans
    equipe[1]["strategie"] = 3  # phantoms

    # init('carte1')
    """
    phantom 1:2 humans 1
    cartes de base, pour permettre de voir la difference entre la strategie 1
    et la strategie 2 au sein de l'equipe des human 
    """
    init('carte2')
    """
    phantom 2 humans 1
    Regardons le phantom le plus haut, puisque il est presuqeu
    arrive a sa destination il ne va pas bouger, il reste 
    en stand by il laisse plutot l'autres agents decaler
    """
    # init('carte3')
    """
    phantom 3 humans 1
    on constate qu'il y a que l'human2 
    qui atteint son objectif. Selon la regle du jeu les phantoms
    sont toujours perdants dans cette disposions. 
    Cependant la strategie3 (rapprochement) permet quand meme
    au phantom de faire un meilleur score en se rapprochant au max de leurs
    cibles 
    [attribuer aux phantom la strategie 1 et remarquons la distance]
    22 au lieu de 26 Donc dans un autres cas de figures la strategie 3 
    de rapprochement permettera a son equipe de gagner en cas d'egalite.
    """
    # init('carte4')
    """
    Et ce qu'on peut le voir dans cette carte, ou dans un premier temps
    phantoms : 3, humans 1
    On peut dire que les phantoms ont largment gagner, Quoique ...
    lorsque les humans applique la strategie cooperative 3 
    les resultats sont presque egales !
    phantoms : 1, humans 3
    """
    # init('carte5')
    """
    phantoms : 1, humans 3
    Les humans gagnets
    phantoms : 3, humans 1
    Les phantoms gagnets
    """


    # init('exAdvCoopMap')
    # init('demoMap')
    # ----------------------------------
    # init('exAdvCoopMap')
    # # ----------------------------------

    
    #-------------------------------
    # Initialisation
    #-------------------------------
    nbLignes = game.spriteBuilder.rowsize
    nbCols = game.spriteBuilder.colsize
    # je recupere tous les joueurs
    players = [o for o in game.layers['joueur']]
    for o in game.layers['joueur']:
        print(blue, o)
        
    nbPlayers = len(players)
    _cardEquipe = nbPlayers // 2
    # positions initiales
    initStates = [o.get_rowcol() for o in game.layers['joueur']]
    # objectifs
    goalStates = [o.get_rowcol() for o in game.layers['ramassable']]
    # obstacles
    wallStates = [w.get_rowcol() for w in game.layers['obstacle']]
    objectifs = goalStates.copy()
    # reperer les murs
    # par defaut la matrice comprend des Tru,gamee
    g = np.ones((nbLignes, nbCols), dtype=bool)
    for w in wallStates:            # putting False for walls
        g[w] = False
    # list des paths
    path = [None] * nbPlayers
    #-------------------------------
    # Strategies
    #-------------------------------
    # # strategie equipe 0 (Human): A* 
    # calcul des paths equipe 0
    print(yellow, "strategie equipe 0 (Human) :", equipe[0]["color"],equipe[0]["strategie"])
    print(yellow, "strategie equipe 1 (Phantom) :", equipe[1]["color"],equipe[1]["strategie"])
    for i in range(0, nbPlayers, 2):
        p = ProblemeGrid2D(initStates[i],objectifs[i],g,'manhattan')
        # path[i] = probleme.astar(p)
        path[i] = probleme.astar(p)
    
    # calcul des paths equipe 1
    for i in range(1, nbPlayers, 2):
        p = ProblemeGrid2D(initStates[i], objectifs[i], g, 'manhattan')
        # path[i] = probleme.astar(p)
        path[i] = probleme.astar(p)

    #-------------------------------
    # Boucle principale de déplacements 
    #-------------------------------    
    ok = [0] * nbPlayers
    bloked = [0] * nbPlayers
    posPlayers = initStates
    cach_posPlayers = []
    # itera = [0 .. 100]
    for itera in range(iterations):
        # a tour de role (pair/impair)
        ie = itera % 2  # indice equipe 0 or 1
        print(yellow,'-----------------------')
        print("Iteration", itera, "les",equipe[ie]["color"], equipe[ie]["name"],yellow,"jouent")
        print('-----------------------',stop)
        ite = (itera-ie)//2   # iteration dans l'equipe
        # on fait jouer tout les joeurs d'une equipe
        for i in range(ie, nbPlayers, 2):
            print(equipe[ie]["color"], "------", itera, "------")
            print(equipe[ie]["name"], i, ":")
            current = posPlayers[i]
            # cas ou le joueur est bloqued des le depart
            if path[i] == []:
                bloked[i] = probleme.distManhattan(current, goalStates[i])
            # si le joueur est arrive ou blocke passez au suivant
            if ok[i] != 0 or bloked[i] != 0:
                print("je passe ! car ", end='')
                if (ok[i] != 0):
                    print("j ai atteint mon objectif", stop)
                    # print(ok)
                else:
                    print(red, "je suis bloqued", stop)
                continue
            
            print("mon path entier :", path[i])
            
            try:
                maybe = row, col = path[i][ite]
            except:
                print(red, "PROBLEME maybe = row, col = path[i][ite]")
                input()

            print("je suis a :", current, "-> je veux ", maybe)


            # NOTARRIVAL CASE 
            print("mon objectif ultime:", goalStates[i])
            if objectifs[i] != goalStates[i]:
                print("mon objectif changed:", objectifs[i])

            
            # NOTARRIVAL CASE
            if equipe[ie]["strategie"] == 3:
                if objectifs[i] != path[i][-1]:
                    print(orange, "de toute maniere je ne peux pas atteindre mon objectif",stop)
                    pac = rapprochement(current, objectifs[i], g)
                    del path[i][ite:]
                    path[i] += pac
                    objectifs[i] = path[i][-1]
                    print("je vais me rapprocher au max suivant ce path:", path[i])
                    maybe = row, col = path[i][ite]
                
            standBy = False  # SADIC CASE
            new_portion = []
            cpt = 0
            while (is_collision(ite, i, maybe, posPlayers, path)):
                # return [] if joueur should stand by
                if equipe[ie]["strategie"] == 1:
                    new_portion = hundle_slicing(g, current, objectifs[i], path[i], ite, wallStates, posPlayers, height)
                elif equipe[ie]["strategie"] == 2:
                    new_portion = hundle_recalcing(g, current, objectifs[i], path[i], ite, height)
                elif equipe[ie]["strategie"] == 3:
                    new_portion = hundle_recalcing(g, current, objectifs[i], path[i], ite, height)
                cpt += 1
                if cpt > 10 :
                    return end("SANS-ISSUE", ie, ok, bloked,game)

                if new_portion != []:
                    print("ok je bouge !")
                    del path[i][ite:]
                    path[i] += new_portion
                    # current = row, col = path[i][ite]
                    maybe = row, col = path[i][ite]
                elif new_portion == []:  # SADIC CASE
                    print("non je stand by")
                    standBy = True
                    break
            
            # si le joueur prefere rester # SADIC CASE
            if standBy:
                print("je reste sur ma case", posPlayers[i])
                continue
            
            current = row, col = path[i][ite]
            posPlayers[i] = (row, col)
            players[i].set_rowcol(row, col)
            print("je suis maintenant sur ma nouvelle case", posPlayers[i])
            # si c la derrniere case dans son path
            if current == path[i][-1]:
                if current == goalStates[i]:
                    ok[i] = -1
                    print(green,"le", equipe[ie]["name"],i, "a atteint son but!",stop)
                    # test(ok)
                    # test(_cardEquipe)
                    # nb_ok = nb(ok)
                    # test(nb_ok)
                    # print("nb(ok[ie::2]) ", nb(ok[ie::2]))
                    # test(_cardEquipe)
                    if( nb(ok[ie::2]) >= _cardEquipe):
                        print("WE-HAVE-A-WINNER !!")
                        return end("WE-HAVE-A-WINNER", ie, ok, bloked,game)
                else:
                    bloked[i] = probleme.distManhattan(current, goalStates[i])
                    print(redbg, equipe[ie]["name"], i, ": Game Over",stop)
                    print(bloked[i])
                    if(nb(bloked[ie::2]) + nb(ok[ie::2]) >= _cardEquipe):
                        print(magentabg, "Les", equipe[ie]["name"], "n'ont plus de coups jouables",stop)
        # si les memebre d'une equipe sont tous soit arrived soit bloqued
        if(nb(bloked) + nb(ok) >= 2 * _cardEquipe):
            print("SITUATION SANS ISSUE: ")
            return end("SANS-ISSUE", ie, ok, bloked,game)
        # raffrechir l'ecran
        # input()
        game.mainiteration()
        if itera == iterations - 1:
            return end("OUT OF ITERATIONS", ie, ok, bloked,game)
        # si plus personne ne bouge arreter le jeu
        if itera % 6 == 0:
            # flag()
            # for i in range(pla)
            if cach_posPlayers == posPlayers:
                return end("OUT OF ITERATIONS", ie, ok, bloked, game)
            cach_posPlayers = posPlayers.copy()
            
    # to delete after that 
    input("Enter to exit")

if __name__ == '__main__':
    try:
        main()
    except Exception as e :
        print("XServer issue "+str(e))
        input()
    


